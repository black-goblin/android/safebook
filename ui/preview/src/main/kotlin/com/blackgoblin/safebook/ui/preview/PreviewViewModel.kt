/*
 * Copyright (c) 2022. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.ui.preview

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.blackgoblin.safebook.domain.usecase.ObserveWordUseCase
import com.blackgoblin.safebook.ui.common.StateViewModel
import com.blackgoblin.safebook.ui.common.ext.update
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class PreviewViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    observeWordUseCase: ObserveWordUseCase
) : StateViewModel<PreviewUiState>(PreviewUiState()) {

    private val wordId: UUID = savedStateHandle.get<String>("wordId").let {
        UUID.fromString(it)
    }

    init {
        viewModelScope.launch {
            observeWordUseCase(wordId).collect {
                _uiState.update { copy(wordResult = it) }
            }
        }
    }
}