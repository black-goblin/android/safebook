/*
 * Copyright (c) 2022. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.ui.preview

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Divider
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.blackgoblin.safebook.domain.Model
import com.blackgoblin.safebook.ui.common.ext.PreviewDevices
import com.blackgoblin.safebook.ui.common.ext.mockedWordResult
import com.blackgoblin.safebook.ui.common.theme.SafebookTheme
import java.util.UUID

@Composable
fun PreviewScreen(
    uiState: PreviewUiState,
    onNavigateToEdit: (UUID) -> Unit = {}
) {
    PreviewContent(uiState, onNavigateToEdit)
}

@Composable
private fun PreviewContent(
    uiState: PreviewUiState,
    onNavigateToEdit: (UUID) -> Unit
) {
    Box {
        uiState.wordResult?.onSuccess { word ->
            LazyColumn(Modifier.fillMaxSize()) {
                item {
                    Text(text = " ${word.value}", style = MaterialTheme.typography.bodyLarge)
                }
                items(word.definitions) {
                    Definition(it)
                    Divider()
                }
            }
            FloatingActionButton(
                modifier = Modifier
                    .align(Alignment.BottomEnd)
                    .padding(16.dp),
                onClick = {
                    onNavigateToEdit(word.id)
                }
            ) {
                Icon(Icons.Default.Edit, contentDescription = "Add")
            }
        }?.onFailure {
            Text(text = "Failure: $it")
        }
    }
}

@Composable
private fun Definition(definition: Model.Definition) {
    Column(Modifier.fillMaxWidth()) {
        Text(text = definition.partOfSpeech?:"", style = MaterialTheme.typography.bodySmall)
        Text(text = definition.value, style = MaterialTheme.typography.bodyMedium)
    }
}

@PreviewDevices
@Composable
private fun PreviewScreenPreview() {
    SafebookTheme {
        PreviewScreen(
            uiState = PreviewUiState(
                wordResult = mockedWordResult
            )
        )
    }
}