/*
 * Copyright (c) 2022. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.ui.preview

import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import java.util.*

fun NavGraphBuilder.previewScreen(
    onNavigateToEdit: (UUID) -> Unit
) {
    composable(
        route = "preview?wordId={wordId}",
        arguments = listOf(navArgument("wordId") {
            type = NavType.StringType
            nullable = false
        })
    ) {
        val viewModel: PreviewViewModel = hiltViewModel()
        val uiState by viewModel.uiState.collectAsState()
        PreviewScreen(
            uiState = uiState,
            onNavigateToEdit = onNavigateToEdit
        )
    }
}

fun NavController.navigateToPreview(id: UUID) {
    navigate(route = "preview?wordId=$id")
}

val SavedStateHandle.wordId: UUID
    get() = checkNotNull(get<String>("wordId")).let { UUID.fromString(it) }