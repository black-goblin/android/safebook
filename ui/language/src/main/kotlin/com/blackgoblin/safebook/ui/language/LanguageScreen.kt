/*
 * Copyright (c) 2025. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.ui.language

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.blackgoblin.safebook.domain.Model
import com.blackgoblin.safebook.ui.common.ext.Empty
import com.blackgoblin.safebook.ui.common.ext.Failure
import com.blackgoblin.safebook.ui.common.ext.PreviewDevices
import com.blackgoblin.safebook.ui.common.ext.mockedFailureResult
import com.blackgoblin.safebook.ui.common.ext.mockedLanguagesEmptyResult
import com.blackgoblin.safebook.ui.common.ext.mockedLanguagesResult
import com.blackgoblin.safebook.ui.common.theme.SafebookTheme
import java.util.UUID

@Composable
fun LanguageScreen(
    uiState: LanguageUiState,
    onAddLanguage: (Model.Language) -> Unit,
    onSelectLanguage: (UUID) -> Unit
) {
    Box(
        Modifier.fillMaxSize()
    ) {
        uiState.languagesResult?.onSuccess { languages ->
            if (languages.isNotEmpty()) {
                LazyColumn(
                    modifier = Modifier.padding(8.dp)
                ) {
                    items(languages) {
                        Language(
                            modifier = Modifier.padding(8.dp),
                            language = it,
                            onSelectLanguage = onSelectLanguage
                        )
                    }
                }
            } else {
                Empty(
                    modifier = Modifier
                        .align(Alignment.Center)
                        .padding(16.dp)
                )
            }
        }?.onFailure { throwable ->
            Failure(
                modifier = Modifier
                    .align(Alignment.Center)
                    .padding(16.dp),
                throwable = throwable
            )
        }
    }
}

@Composable
private fun Language(
    modifier: Modifier = Modifier,
    language: Model.Language,
    onSelectLanguage: (UUID) -> Unit
) {
    ElevatedCard(
        modifier = modifier
            .clickable { onSelectLanguage(language.id) }
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ) {
            Text(
                text = language.value,
                style = MaterialTheme.typography.bodyLarge
            )
        }
    }
}

@PreviewDevices
@Composable
private fun SuccessContentPreview() {
    SafebookTheme {
        LanguageScreen(
            uiState = LanguageUiState(
                mockedLanguagesResult
            ),
            onAddLanguage = {},
            onSelectLanguage = {}
        )
    }
}

@PreviewDevices
@Composable
private fun FailureContentPreview() {
    SafebookTheme {
        LanguageScreen(
            uiState = LanguageUiState(
                mockedFailureResult()
            ),
            onAddLanguage = {},
            onSelectLanguage = {}
        )
    }
}

@PreviewDevices
@Composable
private fun EmptyContentPreview() {
    SafebookTheme {
        LanguageScreen(
            uiState = LanguageUiState(
                mockedLanguagesEmptyResult
            ),
            onAddLanguage = {},
            onSelectLanguage = {}
        )
    }
}