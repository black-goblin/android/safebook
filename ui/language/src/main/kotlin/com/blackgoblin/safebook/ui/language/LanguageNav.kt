/*
 * Copyright (c) 2025. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.ui.language

import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import java.util.UUID

fun NavGraphBuilder.languageScreen(
    onSelectLanguage: (UUID) -> Unit
) {
    composable(
        route = "language"
    ) {
        val viewModel: LanguageViewModel = hiltViewModel()
        val uiState by viewModel.uiState.collectAsState()
        LanguageScreen(
            uiState = uiState,
            onAddLanguage = { viewModel.addLanguage(it) },
            onSelectLanguage = onSelectLanguage
        )
    }
}