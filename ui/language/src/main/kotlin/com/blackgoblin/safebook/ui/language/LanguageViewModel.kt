/*
 * Copyright (c) 2025. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.ui.language

import androidx.lifecycle.viewModelScope
import com.blackgoblin.safebook.domain.Model
import com.blackgoblin.safebook.domain.Model.Language
import com.blackgoblin.safebook.domain.usecase.ObserveLanguagesUseCase
import com.blackgoblin.safebook.ui.common.StateViewModel
import com.blackgoblin.safebook.ui.common.ext.update
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LanguageViewModel @Inject constructor(
    observeLanguagesUseCase: ObserveLanguagesUseCase
) : StateViewModel<LanguageUiState>(LanguageUiState()) {

    init {
        viewModelScope.launch {
            observeLanguagesUseCase(Unit).collect {
                _uiState.update { copy(languagesResult = it) }
            }
        }
    }

    fun addLanguage(language: Language) {}
}