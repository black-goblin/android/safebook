/*
 * Copyright (c) 2022. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.ui.save

import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import java.util.UUID

fun NavGraphBuilder.saveScreen(
    onNavigateBack: () -> Unit
) {
    composable(
        route = "save?wordId={wordId}",
        arguments = listOf(navArgument(name = "wordId") {
            type = NavType.StringType
            nullable = true
        })
    ) {
        val viewModel: SaveViewModel = hiltViewModel()
        val uiState by viewModel.uiState.collectAsState()

        if (uiState.saveResult?.isSuccess == true) {
            onNavigateBack()
        }

        SaveScreen(
            uiState = uiState,
            onNavigateBack = { onNavigateBack() },
            onUpdateWord = { word ->
                viewModel.updateWord(word)
            },
            onSaveWordClick = {
                viewModel.saveWord()
            }
        )
    }
}

fun NavController.navigateToSave(wordId: UUID? = null) {
    val route = wordId?.let { "save?wordId=$it" } ?: "save"
    navigate(route = route)
}

val SavedStateHandle.wordId: UUID?
    get() = get<String?>("wordId")?.let { UUID.fromString(it) }