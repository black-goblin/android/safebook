/*
 * Copyright (c) 2022. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.ui.save

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.blackgoblin.safebook.domain.Model
import com.blackgoblin.safebook.domain.usecase.ObserveWordUseCase
import com.blackgoblin.safebook.domain.usecase.SaveWordUseCase
import com.blackgoblin.safebook.ui.common.StateViewModel
import com.blackgoblin.safebook.ui.common.ext.update
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import java.util.UUID
import javax.inject.Inject

@HiltViewModel
class SaveViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    private val observeWordUseCase: ObserveWordUseCase,
    private val saveWordUseCase: SaveWordUseCase
) : StateViewModel<SaveUiState>(SaveUiState()) {

    private val wordId: UUID? = savedStateHandle.wordId

    init {
        viewModelScope.launch {
            val (draftWord: Model.Word?, failure: Throwable?) = wordId?.let {
                val result = observeWordUseCase(it).first()
                result.getOrNull() to result.exceptionOrNull()
            } ?: run {
                newWord() to null
            }

            _uiState.update {
                copy(
                    draftWord = draftWord,
                    fetchFailure = failure,
                )
            }
        }
    }

    fun updateWord(word: Model.Word) {
        _uiState.update { copy(draftWord = word) }
    }

    fun saveWord() {
        val word = _uiState.value.draftWord ?: return
        viewModelScope.launch {
            val result = saveWordUseCase(word)
            _uiState.update { copy(saveResult = result) }
        }
    }

    private fun newWord() = Model.Word(
        id = UUID.randomUUID(),
        value = "",
        language = null,
        definitions = listOf(
            Model.Definition(
                id = UUID.randomUUID(),
                value = "",
                pronunciation = "",
                partOfSpeech = ""
            )
        )
    )
}