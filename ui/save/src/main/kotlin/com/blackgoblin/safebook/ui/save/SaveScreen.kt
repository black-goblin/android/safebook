/*
 * Copyright (c) 2020. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:OptIn(ExperimentalMaterial3Api::class)

package com.blackgoblin.safebook.ui.save

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import com.blackgoblin.safebook.domain.Model
import com.blackgoblin.safebook.domain.Model.Language
import com.blackgoblin.safebook.ui.common.ext.Failure
import com.blackgoblin.safebook.ui.common.ext.PreviewDevices
import com.blackgoblin.safebook.ui.common.ext.mockedWord1
import com.blackgoblin.safebook.ui.common.theme.SafebookTheme
import kotlinx.coroutines.launch
import java.util.UUID

@Composable
fun SaveScreen(
    uiState: SaveUiState,
    onNavigateBack: () -> Unit,
    onUpdateWord: (Model.Word) -> Unit,
    onSaveWordClick: () -> Unit
) {
    val snackbarHostState = remember { SnackbarHostState() }
    val scope = rememberCoroutineScope()

    Scaffold(
        modifier = Modifier.fillMaxSize(),
        topBar = {
            TopAppBar(
                title = { Text(text = "Create new word") },
                navigationIcon = {
                    IconButton(onClick = { onNavigateBack() }) {
                        Icon(
                            imageVector = Icons.Default.Close,
                            contentDescription = "Close"
                        )
                    }
                },
                actions = {
                    Button(
                        modifier = Modifier.padding(end = 8.dp),
                        enabled = uiState.draftWord?.value?.isNotBlank() == true,
                        onClick = { onSaveWordClick() }) {
                        Text(
                            text = stringResource(id = R.string.label_save)
                        )
                        Spacer(modifier = Modifier.width(8.dp))
                        Icon(
                            imageVector = Icons.Default.Done,
                            contentDescription = null
                        )
                    }
                }
            )
        },
        snackbarHost = {
            SnackbarHost(snackbarHostState)
        }
    ) { innerPaddings ->
        uiState.fetchFailure?.let { throwable ->
            Failure(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                throwable = throwable
            )
        }
        uiState.draftWord?.let { word ->
            Column(
                modifier = Modifier
                    .padding(innerPaddings)
                    .fillMaxSize()
                    .verticalScroll(rememberScrollState())
            ) {
                Word(
                    value = word.value,
                    language = word.language,
                    onWordChange = { onUpdateWord(word.copy(value = it)) },
                    onLanguageChange = { onUpdateWord(word.copy(language = it)) }
                )
                Spacer(
                    modifier = Modifier
                        .height(16.dp)
                        .fillMaxWidth()
                )
                Definitions(
                    definitions = word.definitions,
                    onDefinitionsChange = {
                        onUpdateWord(word.copy(definitions = it))
                    }
                )
            }
        }

        uiState.saveResult?.onFailure {
            scope.launch { snackbarHostState.showSnackbar(message = "Save failure: $it") }
        }
    }
}

@Composable
private fun Word(
    value: String,
    language: Language?,
    onWordChange: (String) -> Unit,
    onLanguageChange: (Language) -> Unit
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
    ) {
        var isExpanded by remember { mutableStateOf(false) }
        val (imageVector, contentDescription) = if (isExpanded) {
            Icons.Default.KeyboardArrowUp to "Fewer word fields"
        } else {
            Icons.Default.KeyboardArrowDown to "More word fields"
        }
        Icon(
            modifier = Modifier.padding(16.dp),
            imageVector = Icons.Default.Edit,
            contentDescription = null
        )
        Column(
            modifier = Modifier.weight(1f),
        ) {
            OutlinedTextField(
                modifier = Modifier.fillMaxWidth(),
                value = value,
                label = { Text(text = "Word") },
                singleLine = true,
                onValueChange = onWordChange
            )
            AnimatedVisibility(visible = isExpanded) {
                OutlinedTextField(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 8.dp),
                    value = "language?.value",
                    label = { Text(text = "Language") },
                    singleLine = true,
                    onValueChange = { /*onLanguageChange(it)*/ }
                )
            }
        }
        IconButton(
            modifier = Modifier.size(56.dp),
            onClick = { isExpanded = isExpanded.not() }) {
            Icon(
                imageVector = imageVector,
                contentDescription = contentDescription
            )
        }
    }
}

@Composable
private fun Definitions(
    definitions: List<Model.Definition>,
    onDefinitionsChange: (List<Model.Definition>) -> Unit
) {
    Column(Modifier.fillMaxWidth()) {
        definitions.forEachIndexed { index, definition ->
            Definition(
                showMoreOptions = definitions.size > 1,
                index = index + 1,
                definition = definition,
                onDefinitionChange = { newDefinition ->
                    onDefinitionsChange(definitions.map {
                        if (it.id == newDefinition.id) {
                            newDefinition
                        } else {
                            it
                        }
                    })
                }
            )
        }
        TextButton(
            modifier = Modifier.padding(16.dp),
            onClick = {
                onDefinitionsChange(
                    definitions.plus(
                        Model.Definition(
                            id = UUID.randomUUID(),
                            value = "",
                            pronunciation = "",
                            partOfSpeech = ""
                        )
                    )
                )
            }
        ) {
            Text(text = "Add more definitions")
        }
    }
}

@Composable
private fun Definition(
    showMoreOptions: Boolean,
    index: Int,
    definition: Model.Definition,
    onDefinitionChange: (Model.Definition) -> Unit
) {
    Row(
        modifier = Modifier.fillMaxWidth(),
    ) {
        Box(
            modifier = Modifier.size(56.dp),
            contentAlignment = Alignment.Center
        ) {
            Icon(
                imageVector = ImageVector.vectorResource(R.drawable.translate),
                contentDescription = null
            )
            if (showMoreOptions) {
                Card(
                    modifier = Modifier
                        .padding(8.dp)
                        .size(16.dp)
                        .align(Alignment.BottomEnd)
                        .clip(CircleShape)
                ) {
                    Box(
                        modifier = Modifier.fillMaxSize(),
                        contentAlignment = Alignment.Center
                    ) {
                        Text(
                            text = index.toString(),
                            style = MaterialTheme.typography.bodySmall
                        )
                    }
                }
            }
        }
        Column(
            modifier = Modifier.weight(1f),
        ) {
            OutlinedTextField(
                modifier = Modifier.fillMaxSize(),
                value = definition.value,
                label = { Text(text = "Definition") },
                singleLine = true,
                onValueChange = {
                    onDefinitionChange(definition.copy(value = it))
                }
            )
        }
        if (showMoreOptions) {
            IconButton(
                modifier = Modifier.size(56.dp),
                onClick = { }) {
                Icon(
                    imageVector = Icons.Default.Close,
                    contentDescription = "Delete definition"
                )
            }
        } else {
            Spacer(modifier = Modifier.size(56.dp))
        }
    }
}

@PreviewDevices
@Composable
private fun SaveScreenPreview() {
    SafebookTheme {
        SaveScreen(
            uiState = SaveUiState(
                draftWord = mockedWord1
            ),
            onNavigateBack = {},
            onUpdateWord = {},
            onSaveWordClick = {}
        )
    }
}