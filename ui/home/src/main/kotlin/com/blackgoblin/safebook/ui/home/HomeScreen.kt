/*
 * Copyright (c) 2022. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.ui.home

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.blackgoblin.safebook.domain.Model
import com.blackgoblin.safebook.ui.common.ext.Empty
import com.blackgoblin.safebook.ui.common.ext.Failure
import com.blackgoblin.safebook.ui.common.ext.PreviewDevices
import com.blackgoblin.safebook.ui.common.ext.mockedFailureResult
import com.blackgoblin.safebook.ui.common.ext.mockedWordsEmptyResult
import com.blackgoblin.safebook.ui.common.ext.mockedWordsResult
import com.blackgoblin.safebook.ui.common.theme.SafebookTheme
import java.util.UUID

@Composable
fun HomeScreen(
    uiState: HomeUiState,
    onNavigateToAdd: () -> Unit,
    onNavigateToPreview: (UUID) -> Unit
) {
    Box(
        Modifier.fillMaxSize()
    ) {
        uiState.wordsResult?.onSuccess { words ->
            if (words.isNotEmpty()) {
                LazyVerticalStaggeredGrid(
                    columns = StaggeredGridCells.Adaptive(172.dp),
                    modifier = Modifier.padding(8.dp)
                ) {
                    items(count = words.size) { index ->
                        val word = words[index]
                        Word(
                            modifier = Modifier.padding(8.dp),
                            word = word,
                            onClick = { onNavigateToPreview(word.id) }
                        )
                    }
                }
            } else {
                Empty(
                    modifier = Modifier
                        .align(Alignment.Center)
                        .padding(16.dp)
                )
            }
        }?.onFailure { throwable ->
            Failure(
                modifier = Modifier
                    .align(Alignment.Center)
                    .padding(16.dp),
                throwable = throwable
            )
        }

        FloatingActionButton(
            modifier = Modifier
                .align(Alignment.BottomEnd)
                .padding(16.dp),
            onClick = onNavigateToAdd
        ) {
            Icon(Icons.Default.Add, contentDescription = "Add")
        }
    }
}

@Composable
private fun Word(
    word: Model.Word,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    ElevatedCard(
        modifier = modifier
            .clickable { onClick() }
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ) {
            Column(
                modifier = Modifier
                    .weight(1f)
            ) {
                Text(
                    text = word.value,
                    style = MaterialTheme.typography.titleLarge,
                    color = MaterialTheme.colorScheme.onSurface
                )

                Spacer(Modifier.height(8.dp))

                word.definitions.take(2).forEach { definition ->
                    Text(
                        text = definition.value,
                        style = MaterialTheme.typography.bodyMedium,
                        color = MaterialTheme.colorScheme.onSurfaceVariant
                    )
                }
            }
            word.language?.let { language ->
                Text(
                    text = language.value,
                    style = MaterialTheme.typography.bodySmall,
                    color = MaterialTheme.colorScheme.onSurfaceVariant
                )
            }
        }
    }
}

@PreviewDevices
@Composable
private fun SuccessContentPreview() {
    SafebookTheme {
        HomeScreen(
            uiState = HomeUiState(
                mockedWordsResult
            ),
            onNavigateToPreview = {},
            onNavigateToAdd = {}
        )
    }
}

@PreviewDevices
@Composable
private fun FailureContentPreview() {
    SafebookTheme {
        HomeScreen(
            uiState = HomeUiState(
                mockedFailureResult()
            ),
            onNavigateToPreview = {},
            onNavigateToAdd = {}
        )
    }
}

@PreviewDevices
@Composable
private fun EmptyContentPreview() {
    SafebookTheme {
        HomeScreen(
            uiState = HomeUiState(
                mockedWordsEmptyResult
            ),
            onNavigateToPreview = {},
            onNavigateToAdd = {}
        )
    }
}