/*
 * Copyright (c) 2022. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.ui.common.ext

import android.content.res.Configuration
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import com.blackgoblin.safebook.domain.Model
import java.util.UUID

@Preview(name = "Light", showBackground = true, backgroundColor = 0xffffff, uiMode = Configuration.UI_MODE_NIGHT_NO)
@Preview(name = "Dark", showBackground = true, backgroundColor = 0x000000, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Preview(name = "Tablet", showBackground = true, backgroundColor = 0xffffff, device = Devices.TABLET)
annotation class PreviewDevices

@Composable
fun Empty(
    modifier: Modifier = Modifier
) {
    Text(
        modifier = modifier,
        text = "No words added yet!",
        textAlign = TextAlign.Center,
        color = MaterialTheme.colorScheme.onBackground
    )
}

@Composable
fun Failure(
    modifier: Modifier = Modifier,
    throwable: Throwable
) {
    Text(
        modifier = modifier,
        text = "Failure: $throwable",
        textAlign = TextAlign.Center,
        color = MaterialTheme.colorScheme.error,
    )
}

val mockedLanguage1 = Model.Language(
    id = UUID.fromString("c3574114-821e-11ed-a1eb-0242ac120002"),
    value = "Danish"
)

val mockedLanguage2 = Model.Language(
    id = UUID.fromString("c3574114-821e-11ed-a1eb-0242ac120003"),
    value = "English"
)

val mockedWord1 = Model.Word(
    id = UUID.fromString("c3574114-821e-11ed-a1eb-0242ac120002"),
    value = "Ordbog",
    language = mockedLanguage1,
    definitions = listOf(
        Model.Definition(
            id = UUID.randomUUID(),
            value = "Dictionary",
            pronunciation = "Uo a bo",
            partOfSpeech = "Noun"
        )
    )
)

val mockedWord2 = Model.Word(
    id = UUID.fromString("c3574114-821e-11ed-a1eb-0242ac120003"),
    value = "Dictionary",
    language = mockedLanguage2,
    definitions = listOf(
        Model.Definition(
            id = UUID.randomUUID(),
            value = "A book with a lot of words in it",
            pronunciation = "",
            partOfSpeech = "Noun"
        ),
        Model.Definition(
            id = UUID.randomUUID(),
            value = "A book of vocabularies",
            pronunciation = "dik-shuh-neh-ree",
            partOfSpeech = "Noun"
        )
    )
)

fun <T> mockedFailureResult() = Result.failure<T>(Exception("Unexpected failure!"))

val mockedLanguageResult: Result<Model.Language> = Result.success(mockedLanguage1)

val mockedLanguagesResult: Result<List<Model.Language>> = Result.success(listOf(mockedLanguage1, mockedLanguage2))

val mockedLanguagesEmptyResult: Result<List<Model.Language>> = Result.success(emptyList())

val mockedWordResult: Result<Model.Word> = Result.success(mockedWord1)

val mockedWordsResult: Result<List<Model.Word>> = Result.success(listOf(mockedWord1, mockedWord2))

val mockedWordsEmptyResult: Result<List<Model.Word>> = Result.success(emptyList())