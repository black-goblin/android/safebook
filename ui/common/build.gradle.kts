/*
 * Copyright (c) 2019. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.plugin.compose")
    kotlin("android")
    kotlin("kapt")
}

android {
    compileSdk = rootProject.extra.get("compileSdkVersion") as Int

    defaultConfig {
        minSdk = rootProject.extra.get("minSdkVersion") as Int
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildFeatures {
        compose = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = "17"
    }

    sourceSets {
        getByName("main").java.srcDirs("src/main/kotlin")
        getByName("androidTest").java.srcDirs("src/androidTest/kotlin")
        getByName("test").java.srcDirs("src/test/kotlin")
    }
    namespace = "com.blackgoblin.safebook.ui.common"
}

dependencies {
    api(project(":domain"))

    implementation(libs.dependency.googleHilt)
    kapt(libs.dependency.googleHiltCompiler)
    api(libs.dependency.googleMaterial)

    api(platform(libs.dependency.androidxComposeBom))
    api(libs.dependency.androidxComposeMaterial)
    api(libs.dependency.androidxComposeMaterialWindow)
    debugApi(libs.dependency.androidxComposeUiTooling)
    api(libs.dependency.androidxComposeUiToolingPreview)

    api(libs.dependency.androidxCorektx)
    api(libs.dependency.androidxAppcompat)
    api(libs.dependency.androidxActivityktx)
    api(libs.dependency.androidxActivityCompose)
    api(libs.dependency.androidxNavigationUiktx)
    api(libs.dependency.androidxNavigationCompose)
    api(libs.dependency.androidxHiltNavigationCompose)
    api(libs.dependency.androidxViewModelktx)
    api(libs.dependency.androidxRoomkRuntime)
    api(libs.dependency.androidxRoomktx)
    kapt(libs.dependency.androidxRoomCompiler)
}