# How to Contribute
You are more than welcome to contribute to the project. Just follow these guides, which is basically a simple `Git Flow`.

## Development
1. Create a task in `Gitlab`, using that create a `Merge Request` from `develop`, this way a branch would be created and you can start working.
2. When committing, make sure to reference the issue/issues you are working on like `#41 fixes navigation bug…`.
2. When you are finished, go to your `Merge Request` and click on `Resolve WIP status` and assign it to someone to review.

## Keys and API Variables
Normally you only ever need to make `stagingDebug` builds for development purposes, which means you don't need to define any variables or add any credential files.
**To make a build with other `Build Variants` you need more credentials. In that case create an issue and state why you need this credentials.**