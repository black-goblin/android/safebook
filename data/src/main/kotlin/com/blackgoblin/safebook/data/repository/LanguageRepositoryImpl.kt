/*
 * Copyright (c) 2024. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.data.repository

import com.blackgoblin.safebook.data.datasource.LanguageLocalDataSource
import com.blackgoblin.safebook.domain.Model.Language
import com.blackgoblin.safebook.domain.repository.LanguageRepository
import kotlinx.coroutines.flow.Flow
import java.util.UUID

class LanguageRepositoryImpl(
    private val languageLocalDataSource: LanguageLocalDataSource
): LanguageRepository {

    override suspend fun persist(language: Language) {
        languageLocalDataSource.persist(language)
    }

    override fun fetch(): Flow<List<Language>> =
        languageLocalDataSource.fetch()

    override fun fetch(id: UUID): Flow<Language> =
        languageLocalDataSource.fetch(id)
}