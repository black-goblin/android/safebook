/*
 * Copyright (c) 2020. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.data.datasource

import com.blackgoblin.safebook.domain.Model
import com.blackgoblin.safebook.domain.Model.Language
import com.blackgoblin.safebook.domain.Model.Word
import kotlinx.coroutines.flow.Flow
import java.util.UUID

/**
 * Represents a [local data source][LocalDataSource] for [words][Word]
 */
interface WordLocalDataSource : LocalDataSource<Word> {

    /**
     * Persists a [word].
     */
    suspend fun persist(word: Word)

    /**
     * Fetches all the [words][Word] based on a [language][Language].
     * null to fetch all languages.
     */
    fun fetch(language: Language? = null): Flow<List<Word>>

    /**
     * Fetches a particular [word][Word] based on its [id].
     */
    fun fetch(id: UUID): Flow<Word>
}