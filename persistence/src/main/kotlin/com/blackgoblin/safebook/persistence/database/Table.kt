/*
 * Copyright (c) 2020. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.persistence.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

sealed class Table {

    @Entity(tableName = "words")
    data class Word(
        @ColumnInfo(name = "id") @PrimaryKey val id: String,
        @ColumnInfo(name = "value") val value: String,
        @ColumnInfo(name = "language_id", index = true) val languageId: String?
    ) : Table()

    @Entity(tableName = "languages")
    data class Language(
        @ColumnInfo(name = "id") @PrimaryKey val id: String,
        @ColumnInfo(name = "value") val value: String
    ) : Table()

    @Entity(tableName = "definitions")
    data class Definition(
        @ColumnInfo(name = "id") @PrimaryKey val id: String,
        @ColumnInfo(name = "value") val value: String,
        @ColumnInfo(name = "part_of_speech") val partOfSpeech: String?,
        @ColumnInfo(name = "pronunciation") val pronunciation: String?,
        @ColumnInfo(name = "word_id", index = true) val wordId: String
    ) : Table()
}