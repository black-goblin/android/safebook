/*
 * Copyright (c) 2024. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.persistence.datasource

import com.blackgoblin.safebook.data.datasource.LanguageLocalDataSource
import com.blackgoblin.safebook.domain.Model.Language
import com.blackgoblin.safebook.persistence.database.LanguageDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.util.UUID

class LanguageLocalDataSourceImpl(
    private val languageDao: LanguageDao
) : LanguageLocalDataSource {

    override suspend fun persist(language: Language) {
        languageDao.insert(language.mapToTable())
    }

    override fun fetch(): Flow<List<Language>> =
        languageDao.select().map { list -> list.map { it.mapToModel() } }

    override fun fetch(id: UUID): Flow<Language> =
        languageDao.select(id.toString()).map { it.mapToModel() }
}