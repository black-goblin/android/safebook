/*
 * Copyright (c) 2020. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.persistence.datasource

import com.blackgoblin.safebook.domain.Model
import com.blackgoblin.safebook.persistence.database.Table
import com.blackgoblin.safebook.persistence.database.WordDao
import java.util.UUID

internal fun Model.Word.mapToTable() = Table.Word(
    id = id.toString(),
    value = value,
    languageId = language?.id?.toString()
)

internal fun Model.Language.mapToTable() = Table.Language(
    id = id.toString(),
    value = value,
)

internal fun Model.Definition.mapToTable(wordId: UUID) = Table.Definition(
    id = id.toString(),
    value = value,
    pronunciation = pronunciation,
    partOfSpeech = partOfSpeech,
    wordId = wordId.toString()
)

internal fun Table.Word.mapToModel(
    language: Model.Language?,
    definitions: List<Model.Definition>
) = Model.Word(
    id = id.toUUID(),
    value = value,
    language = language,
    definitions = definitions
)

internal fun Table.Language.mapToModel() = Model.Language(
    id = id.toUUID(),
    value = value,
)

internal fun WordDao.WordLanguageDefinitions.mapToModel() = Model.Word(
    id = word.id.toUUID(),
    value = word.value,
    language = language.mapToModel(),
    definitions = definitions.map { it.mapToModel() }
)

private fun Table.Definition.mapToModel() = Model.Definition(
    id = id.toUUID(),
    value = value,
    pronunciation = pronunciation,
    partOfSpeech = partOfSpeech
)

private fun String.toUUID() =
    UUID.fromString(this)