/*
 * Copyright (c) 2020. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.persistence.datasource

import com.blackgoblin.safebook.data.datasource.WordLocalDataSource
import com.blackgoblin.safebook.domain.Model.Language
import com.blackgoblin.safebook.domain.Model.Word
import com.blackgoblin.safebook.persistence.database.DefinitionDao
import com.blackgoblin.safebook.persistence.database.WordDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.util.UUID

class WordLocalDataSourceImpl(
    private val wordDao: WordDao,
    private val definitionDao: DefinitionDao
) : WordLocalDataSource {

    override suspend fun persist(word: Word) {
        val wordId = word.id
        val definitions = word.definitions.map { it.mapToTable(wordId) }
        wordDao.insert(word.mapToTable())
        definitionDao.insert(definitions)
    }

    override fun fetch(language: Language?): Flow<List<Word>> =
        language?.let {
            wordDao.selectWhere(language.mapToTable().id).map { list -> list.map { it.mapToModel(language, emptyList()) } }
        } ?: run {
            wordDao.select().map { list -> list.map { it.mapToModel(null, emptyList()) } }
        }

    override fun fetch(id: UUID): Flow<Word> =
        wordDao.select(id.toString()).map { it.mapToModel() }
}