/*
 * Copyright (c) 2020. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.persistence.database

import androidx.room.Dao
import androidx.room.Embedded
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query
import androidx.room.Relation
import androidx.room.Transaction
import com.blackgoblin.safebook.persistence.database.Table.Definition
import com.blackgoblin.safebook.persistence.database.Table.Language
import com.blackgoblin.safebook.persistence.database.Table.Word
import kotlinx.coroutines.flow.Flow

@Dao
interface WordDao {

    @Insert(onConflict = REPLACE)
    suspend fun insert(word: Word)

    @Insert(onConflict = REPLACE)
    suspend fun insert(definitions: List<Definition>)

    @Query("SELECT * FROM words")
    fun select(): Flow<List<Word>>

    @Query("SELECT * FROM words WHERE language_id = :languageId")
    fun selectWhere(languageId: String): Flow<List<Word>>

    @Transaction
    @Query("SELECT * FROM words WHERE id = :id")
    fun select(id: String): Flow<WordLanguageDefinitions>

    data class WordLanguageDefinitions(
        @Embedded val word: Word,
        @Relation(
            parentColumn = "language_id",
            entityColumn = "id"
        )
        val language: Language,
        @Relation(
            parentColumn = "id",
            entityColumn = "word_id"
        )
        val definitions: List<Definition>
    )
}