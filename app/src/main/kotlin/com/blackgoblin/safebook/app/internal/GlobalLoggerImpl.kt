/*
 * Copyright (c) 2020. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.app.internal

import android.util.Log.*
import com.blackgoblin.safebook.domain.GlobalLogger
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.crashlytics.FirebaseCrashlytics

class GlobalLoggerImpl(
    private val isLocalLogEnabled: Boolean,
    private val analytics: FirebaseAnalytics,
    private val crashlytics: FirebaseCrashlytics
) : GlobalLogger {

    override fun v(tag: String, message: String, throwable: Throwable?) =
        log(VERBOSE, tag, message, throwable)

    override fun d(tag: String, message: String, throwable: Throwable?) =
        log(DEBUG, tag, message, throwable)

    override fun i(tag: String, message: String, throwable: Throwable?) =
        log(INFO, tag, message, throwable)

    override fun w(tag: String, message: String, throwable: Throwable?) =
        log(WARN, tag, message, throwable)

    override fun e(tag: String, message: String, throwable: Throwable?) =
        log(ERROR, tag, message, throwable)

    override fun wtf(tag: String, message: String, throwable: Throwable?) =
        log(ASSERT, tag, message, throwable)

    override fun log(level: Int, tag: String, message: String, throwable: Throwable?) {
        val msg = message + getStackTraceString(throwable)

        if (isLocalLogEnabled) {
            println(level, tag, msg)
        } else {
            crashlytics.log("${level.levelName}/$tag: $msg")
        }
    }

    private val Int.levelName: String
        get() {
            return when (this) {
                VERBOSE -> "V"
                DEBUG -> "D"
                INFO -> "I"
                WARN -> "W"
                ERROR -> "E"
                ASSERT -> "A"
                else -> "U"
            }
        }

    override fun setDataCollectionEnabled(isEnabled: Boolean) {
        analytics.setAnalyticsCollectionEnabled(isEnabled)
        crashlytics.setCrashlyticsCollectionEnabled(isEnabled)
    }

    override fun setUserId(id: String) {
        analytics.setUserId(id)
        crashlytics.setUserId(id)
    }
}