/*
 * Copyright (c) 2020. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.app.di

import android.content.Context
import androidx.room.Room
import com.blackgoblin.safebook.persistence.database.DefinitionDao
import com.blackgoblin.safebook.persistence.database.LanguageDao
import com.blackgoblin.safebook.persistence.database.SafebookDatabase
import com.blackgoblin.safebook.persistence.database.WordDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class PersistenceModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): SafebookDatabase =
        Room.databaseBuilder(context, SafebookDatabase::class.java, "database.db").build()

    @Provides
    @Singleton
    fun provideWordDao(database: SafebookDatabase): WordDao =
        database.getWordDao()

    @Provides
    @Singleton
    fun provideLanguageDao(database: SafebookDatabase): LanguageDao =
        database.getLanguageDao()

    @Provides
    @Singleton
    fun provideDefinitionDao(database: SafebookDatabase): DefinitionDao =
        database.getDefinitionDao()
}