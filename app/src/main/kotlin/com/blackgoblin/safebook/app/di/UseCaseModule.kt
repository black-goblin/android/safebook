/*
 * Copyright (c) 2020. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.app.di

import com.blackgoblin.safebook.domain.repository.LanguageRepository
import com.blackgoblin.safebook.domain.repository.WordRepository
import com.blackgoblin.safebook.domain.usecase.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Provides
    fun provideAddWordUseCase(wordRepository: WordRepository, dispatcher: CoroutineDispatcher): SaveWordUseCase =
        SaveWordUseCase(wordRepository, dispatcher)

    @Provides
    fun provideObserveWordsUseCase(wordRepository: WordRepository, dispatcher: CoroutineDispatcher): ObserveWordsUseCase =
        ObserveWordsUseCase(wordRepository, dispatcher)

    @Provides
    fun provideObserveWordUseCase(wordRepository: WordRepository, dispatcher: CoroutineDispatcher): ObserveWordUseCase =
        ObserveWordUseCase(wordRepository, dispatcher)

    @Provides
    fun provideObserveLanguagesUseCase(wordRepository: LanguageRepository, dispatcher: CoroutineDispatcher): ObserveLanguagesUseCase =
        ObserveLanguagesUseCase(wordRepository, dispatcher)
}