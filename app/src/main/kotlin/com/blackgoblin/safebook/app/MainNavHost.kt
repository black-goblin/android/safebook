/*
 * Copyright (c) 2022. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.app

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.blackgoblin.safebook.ui.home.homeScreen
import com.blackgoblin.safebook.ui.preview.navigateToPreview
import com.blackgoblin.safebook.ui.preview.previewScreen
import com.blackgoblin.safebook.ui.save.navigateToSave
import com.blackgoblin.safebook.ui.save.saveScreen

@Composable
fun MainNavHost(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController(),
    startDestination: String = "home"
) {
    NavHost(
        modifier = modifier,
        navController = navController,
        startDestination = startDestination
    ) {
        homeScreen(
            onNavigateToAdd = { navController.navigateToSave() },
            onNavigateToPreview = { navController.navigateToPreview(it) }
        )
        saveScreen(
            onNavigateBack = { navController.popBackStack() }
        )
        previewScreen(
            onNavigateToEdit = { navController.navigateToSave(it) }
        )
    }
}