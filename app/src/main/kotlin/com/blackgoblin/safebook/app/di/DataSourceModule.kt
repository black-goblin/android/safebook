/*
 * Copyright (c) 2020. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.app.di

import com.blackgoblin.safebook.data.datasource.LanguageLocalDataSource
import com.blackgoblin.safebook.data.datasource.WordLocalDataSource
import com.blackgoblin.safebook.persistence.database.DefinitionDao
import com.blackgoblin.safebook.persistence.database.LanguageDao
import com.blackgoblin.safebook.persistence.database.WordDao
import com.blackgoblin.safebook.persistence.datasource.LanguageLocalDataSourceImpl
import com.blackgoblin.safebook.persistence.datasource.WordLocalDataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataSourceModule {

    @Provides
    @Singleton
    fun provideWordLocalDataSource(dao: WordDao, definitionDao: DefinitionDao): WordLocalDataSource =
        WordLocalDataSourceImpl(dao, definitionDao)

    @Provides
    @Singleton
    fun provideLanguageLocalDataSource(languageDao: LanguageDao): LanguageLocalDataSource =
        LanguageLocalDataSourceImpl(languageDao)
}