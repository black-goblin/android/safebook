# http://developer.android.com/guide/developing/tools/proguard.html

# Crashlytics
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-renamesourcefileattribute SourceFile
-keep public class * extends java.lang.Exception