/*
 * Copyright (c) 2019. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

plugins {
    id("com.android.application")
    kotlin("android")
    id("dagger.hilt.android.plugin")
    id("com.google.firebase.appdistribution")
    id("com.github.triplet.play")
    id("org.jetbrains.kotlin.plugin.compose")
    kotlin("kapt")
}

fun Project.getPropertyOrEnvironmentVariable(key: String): String? =
    properties[key]?.toString() ?: System.getenv(key)

android {
    println(extra.properties)
    compileSdk = rootProject.extra.get("compileSdkVersion") as Int

    defaultConfig {
        applicationId = "com.blackgoblin.safebook"
        minSdk = rootProject.extra.get("minSdkVersion") as Int
        targetSdk = rootProject.extra.get("targetSdkVersion") as Int
        versionCode = 5
        versionName = "0.9.0"

        base.archivesName.set("${applicationId}-${versionName}-${versionCode}}")
    }

    buildFeatures {
        compose = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = "17"
    }

    signingConfigs {
        getByName("debug") {
            storeFile = rootProject.file("credentials/debug.jks")
            storePassword = "debugStorePassword"
            keyAlias = "debugKeyAlias"
            keyPassword = "debugKeyPassword"
        }

        create("release") {
            storeFile = rootProject.file("credentials/release.jks")
            storePassword = getPropertyOrEnvironmentVariable("STORE_PASSWORD")
            keyAlias = getPropertyOrEnvironmentVariable("KEY_ALIAS")
            keyPassword = getPropertyOrEnvironmentVariable("KEY_PASSWORD")
        }
    }

    buildTypes {
        getByName("debug") {
            rootProject.extra.set("enableCrashlytics", false)
            rootProject.extra.set("alwaysUpdateBuildId", false)

            isDebuggable = true
            isMinifyEnabled = false
            isShrinkResources = false
            applicationIdSuffix = ".debug"
            versionNameSuffix = "-DEBUG"
        }

        getByName("release") {
            isMinifyEnabled = true
            isShrinkResources = true
            isDebuggable = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )

            firebaseAppDistribution {
                serviceCredentialsFile = "./credentials/firebase-app-distribution.json"
            }
        }
    }

    productFlavors {
        flavorDimensions.add("environment")

        create("staging") {
            applicationIdSuffix = ".staging"
            versionNameSuffix = "-STAGING"
            signingConfig = signingConfigs.getByName("debug")
        }

        create("production") {
            signingConfig = signingConfigs.getByName("release")
        }
    }

    sourceSets {
        getByName("main").java.srcDirs("src/main/kotlin")
        getByName("androidTest").java.srcDirs("src/androidTest/kotlin")
        getByName("test").java.srcDirs("src/test/kotlin")
    }
    namespace = "com.blackgoblin.safebook.app"
    buildFeatures {
        buildConfig = true
    }

    androidComponents {
        beforeVariants(selector().all()) { variant ->
            if (variant.buildType == "debug" && variant.flavorName == "production") {
                variant.enable = false
            }
        }
    }
}

play {
    track.set("internal")
    defaultToAppBundles.set(true)
    serviceAccountCredentials.set(rootProject.file("credentials/play-publisher.json"))
}

dependencies {
    implementation(project(":domain"))
    implementation(project(":data"))
    implementation(project(":persistence"))
    implementation(project(":ui:common"))
    implementation(project(":ui:home"))
    implementation(project(":ui:save"))
    implementation(project(":ui:language"))
    implementation(project(":ui:preview"))

    implementation(libs.dependency.googleHilt)
    kapt(libs.dependency.googleHiltCompiler)

    implementation(libs.dependency.googleCrashlytics)
    implementation(libs.dependency.googleAnalytics)
}

if (file("google-services.json").exists()) {
    apply(plugin = "com.google.gms.google-services")
    apply(plugin = "com.google.firebase.crashlytics")
}