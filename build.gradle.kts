/*
 * Copyright (c) 2019. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.commons.codec.binary.Base64
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinJvmCompile
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.FileInputStream
import java.io.FileOutputStream
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

buildscript {
    extra.apply {
        set("compileSdkVersion", 34)
        set("minSdkVersion", 21)
        set("targetSdkVersion", 34)
        set("composeCompilerVersion", "1.4.8")
    }

    repositories {
        google()
        mavenCentral()
    }

    dependencies {
        classpath(libs.plugin.android)
        classpath(libs.plugin.kotlin)
        classpath(libs.plugin.comoseCompiler)
        classpath(libs.plugin.googleServies)
        classpath(libs.plugin.crashlytics)
        classpath(libs.plugin.hilt)
        classpath(libs.plugin.appDistribution)
        classpath(libs.plugin.playPublisher)
    }
}

allprojects {
    repositories {
        google()
        mavenCentral()
    }
    tasks.withType<KotlinJvmCompile>().configureEach {
        compilerOptions {
            jvmTarget.set(JvmTarget.JVM_17)
        }
    }}

tasks.register("clean", Delete::class) {
    delete(rootProject.layout.buildDirectory)
}

afterEvaluate {
    val credentialsKey = getPropertyOrEnvironmentVariable("CREDENTIALS_KEY")
    val credentialsInitVector = getPropertyOrEnvironmentVariable("CREDENTIALS_INITIAL_VECTOR")

    if (credentialsKey == null || credentialsInitVector == null) return@afterEvaluate

    handleCredentials(
        "credentials/release.aes",
        "credentials/release.jks",
        credentialsKey,
        credentialsInitVector
    )

    handleCredentials(
        "credentials/google-services.aes",
        "app/google-services.json",
        credentialsKey,
        credentialsInitVector
    )

    handleCredentials(
        "credentials/play-publisher.aes",
        "credentials/play-publisher.json",
        credentialsKey,
        credentialsInitVector
    )

    handleCredentials(
        "credentials/firebase-app-distribution.aes",
        "credentials/firebase-app-distribution.json",
        credentialsKey,
        credentialsInitVector
    )
}

fun handleCredentials(
    encryptedPath: String,
    decryptedPath: String,
    credentialsKey: String,
    credentialsInitVector: String
) {
    val encryptedFile = File(encryptedPath)
    if (encryptedFile.exists()) {
        val encrypted = readFile(encryptedFile)
        val encoded = Base64.encodeBase64String(encrypted)
        val byteArray = decrypt(encoded, credentialsKey, credentialsInitVector)
        saveFile(byteArray, decryptedPath)
    } else {
        val keystore = readFile(File(decryptedPath))
        val encrypted = encrypt(keystore, credentialsKey, credentialsInitVector)
        saveFile(Base64.decodeBase64(encrypted), encryptedPath)
    }
}

fun readFile(file: File): ByteArray {
    val fileContents = file.readBytes()
    val inputBuffer = BufferedInputStream(FileInputStream(file))

    inputBuffer.read(fileContents)
    inputBuffer.close()

    return fileContents
}

fun encrypt(value: ByteArray, key: String, initialVector: String): String {
    val iv = IvParameterSpec(initialVector.toByteArray(Charsets.UTF_8))
    val keySpec = SecretKeySpec(key.toByteArray(Charsets.UTF_8), "AES")

    val cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING")
    cipher.init(Cipher.ENCRYPT_MODE, keySpec, iv)

    val encrypted = cipher.doFinal(value)
    return Base64.encodeBase64String(encrypted)
}

fun decrypt(value: String, key: String, initialVector: String): ByteArray {
    val iv = IvParameterSpec(initialVector.toByteArray(Charsets.UTF_8))
    val keySpec = SecretKeySpec(key.toByteArray(Charsets.UTF_8), "AES")

    val cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING")
    cipher.init(Cipher.DECRYPT_MODE, keySpec, iv)

    val decoded = Base64.decodeBase64(value)

    return cipher.doFinal(decoded)
}

fun saveFile(fileData: ByteArray, path: String) {
    val file = File(path)
    val bos = BufferedOutputStream(FileOutputStream(file, false))
    bos.write(fileData)
    bos.flush()
    bos.close()
}

fun Project.getPropertyOrEnvironmentVariable(key: String): String? =
    properties[key]?.toString() ?: System.getenv(key)