# Safebook
The first step to learn a new language is to add to your vocabulary and learn the grammar.
Safebook is a notebook for you to save your vocabulary and grammar rules.

## Architecture
The architecture is based on Clean Architecture principals with help of Google's `MVVM`.

1. Domain: Consists of `Use Case`s and shared models or `Entity`s.
2. Data: The business code and the implementation of the Domain layer, `Interface Adapters`.
3. View: This layer uses Domain layer interfaces along with their implementations from Data layer, using Dependency Injection.