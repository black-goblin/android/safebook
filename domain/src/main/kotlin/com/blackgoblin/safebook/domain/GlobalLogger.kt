/*
 * Copyright (c) 2019. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.domain

/**
 * Global logger. This logger must be used to ensure the logs can be
 * disabled/enabled and/or uploaded when needed.
 *
 * Note that none of the functions are synchronized.
 */
interface GlobalLogger {

    /**
     * Logs a verbose
     */
    fun v(tag: String, message: String, throwable: Throwable? = null)

    /**
     * Logs a debug
     */
    fun d(tag: String, message: String, throwable: Throwable? = null)

    /**
     * Logs a info
     */
    fun i(tag: String, message: String, throwable: Throwable? = null)

    /**
     * Logs a warning
     */
    fun w(tag: String, message: String, throwable: Throwable? = null)

    /**
     * Logs a error
     */
    fun e(tag: String, message: String, throwable: Throwable? = null)

    /**
     * Logs an assertion
     */
    fun wtf(tag: String, message: String, throwable: Throwable? = null)

    /**
     * Logs with the desired [log level][level]
     */
    fun log(level: Int, tag: String, message: String, throwable: Throwable? = null)

    /**
     * Enables data collection like analytics and crash reporting.
     * Enabled by default.
     */
    fun setDataCollectionEnabled(isEnabled: Boolean)

    /**
     * Sets an id to the user to find their collected data later.
     * Enabled by default.
     */
    fun setUserId(id: String)
}