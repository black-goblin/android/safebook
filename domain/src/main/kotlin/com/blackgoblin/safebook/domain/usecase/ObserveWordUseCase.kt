/*
 * Copyright (c) 2021. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.domain.usecase

import com.blackgoblin.safebook.domain.Model.Word
import com.blackgoblin.safebook.domain.repository.WordRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import java.util.*

/**
 * Observes a particular [word][Word] based on its id.
 */
class ObserveWordUseCase(
    private val wordRepository: WordRepository,
    dispatcher: CoroutineDispatcher
) : FlowUseCase<UUID, Word>(dispatcher) {

    override fun execute(input: UUID): Flow<Word> =
        wordRepository.fetch(input)
}