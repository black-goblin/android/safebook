/*
 * Copyright (c) 2020. Black-goblin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blackgoblin.safebook.domain.usecase

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map

/**
 * Executes business logic in its [execute] function and keep posting updates to the result as
 * [Result<R>].
 * Handling an exception (emit [Result.Failure] to the result) is the subclasses's responsibility.
 */
abstract class FlowUseCase<Input, Output>(
    private val dispatcher: CoroutineDispatcher
) : UseCase<Input, Flow<Output>> {

    @Suppress("EXPERIMENTAL_API_USAGE")
    operator fun invoke(input: Input): Flow<Result<Output>> =
        execute(input)
            .map { Result.success(it) }
            .catch { e -> emit(Result.failure(e)) }
            .flowOn(dispatcher)

    protected abstract fun execute(input: Input): Flow<Output>
}